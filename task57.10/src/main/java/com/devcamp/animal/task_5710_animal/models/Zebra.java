package com.devcamp.animal.task_5710_animal.models;

public class Zebra extends Animal {
    private boolean is_wild;

    public Zebra(boolean is_wild) {
        this.is_wild = is_wild;
    }

    public Zebra(int age, String gender, boolean is_wild) {
        super(age, gender);
        this.is_wild = is_wild;
    }

    @Override
    public void isMammal() {
        // TODO Auto-generated method stub
        System.out.println("Zebra is mammal!");  
    }
    
    public void run() {
        System.out.println("Zebra running ...");
    }

    public boolean isIs_wild() {
        return is_wild;
    }

    public void setIs_wild(boolean is_wild) {
        this.is_wild = is_wild;
    }
}
