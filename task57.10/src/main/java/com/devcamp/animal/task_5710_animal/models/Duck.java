package com.devcamp.animal.task_5710_animal.models;

public class Duck extends Animal {
    private String beakColor;

    public Duck() {
        super();
    }

    public Duck(String beakColor) {
        super();
        this.beakColor = beakColor;
    }

    public Duck(int age, String gender, String beakColor) {
        super(age, gender);
        this.beakColor = beakColor;
    }

    @Override
    public void isMammal() {
        // TODO Auto-generated method stub
        System.out.println("Duck is mammal!");        
    }

    public void swim() {
        System.out.println("Duck swimming ...");
    }
    
    public void quack() {
        System.out.println("Duck quacking ...");
    }

    public String getBeakColor() {
        return beakColor;
    }

    public void setBeakColor(String beakColor) {
        this.beakColor = beakColor;
    }

    public void mate() {
        System.out.println("Duck mating ...");
    }
}
